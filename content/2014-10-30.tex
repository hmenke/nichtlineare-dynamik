\subsection{Bernoulli-Verschiebung}
\index{Bernoulli-Verschiebung}

Die Vorschrift lautet
\begin{equation*}
  x_{n+1} = 2 x_n \bmod 1.
\end{equation*}
Dies ist eine Abbildung $[0,1) \to [0,1)$ (siehe
Abbildung~\ref{fig:3.1}).

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,xscale=3,yscale=2]
    \draw[<->] (0,1.5) node[right] {$f(x_n)$} |- (1.5,0) node[below] {$x_n$};
    \draw[MidnightBlue] plot[domain=0:1,samples=100] (\x,{mod(2*\x,1)});
    \foreach \i in {{1/2},1} {
      \draw (\i,2pt) -- (\i,-2pt) node[below] {$\i$};
    }
  \end{tikzpicture}
  \caption{Skizze der Bernoulli-Abbildung. Man erkennt die Streckung
    und Faltung des Einheitsintervalls.}
  \label{fig:3.1}
\end{figure}

Die Vorschift $f(x)$ ist eine Verschiebung der $(0,1)$-Folge in
binärer Zahlendarstellung. Dies wird Bernoulli-Shift genannt.
\begin{align*}
  x_n &= 0.0111000101\ldots \\
  x_{n+1} &= 0.111000101\ldots
\end{align*}

Die Eigenschaften dieser Abbildung sind
\begin{itemize}
\item $x_0$ ist eine rationale Zahl $\implies$ $x_n$ ist eine
  periodische Folge
\item $x_0$ ist irrational $\implies$ Die Folge $x_n$ ist ergodisch,
  d.h.\ die $x_n$ kommen jedem Wert in $[0,1)$ beliebig oft beliebig
  nahe.
\item Sensitive Abhängigkeit der Folge vom Anfangswert $x_0$.
\end{itemize}

Betrachte die Folge $x_n$ mit Anfangswerten $x_0$, $x_0+\varepsilon$
mit $\varepsilon \in [2^{-N},2^{-N+1}]$. Nach $N$ Iterationen
unterscheiden sich die Folgen vollständig. Dies entspricht einer
Fehlerverstärkung um den Faktor $2$ bei jeder Iteration
(Streckung). Der Fehler wächst also exponentiell mit der Zahl der
Iterationen.

\begin{theorem}[Definition]
  Der \acct{Liapunovexponent} ist definiert durch:
  \begin{center}
    \begin{tikzpicture}
      \draw[|-|] (0,0) node[below] {$x_0$} -- node[above] {$\varepsilon$} (1,0) node[below] {$x_0+\varepsilon$};
      \node at (2.5,0) {$\overset{\text{$n$ Iterationen}}{\implies}$};
      \draw[|-|] (4,0) node[below] {$f^n(x_0)$} -- node[above] {$\varepsilon \ee^{n \lambda(x_0)}$} (7,0) node[below] {$f^n(x_0+\varepsilon)$};
    \end{tikzpicture}
  \end{center}
  wobei
  \begin{equation*}
    f^{n+1}(x) = f(f^n(x))
  \end{equation*}
  Damit folgt die Definition des Liapunovexponenten:
  \begin{align*}
    \lambda(x_0) &= \lim_{n\to\infty}\lim_{\varepsilon\to0} \frac{1}{n}
    \ln \left| \frac{f^n(x_0+\varepsilon)-f^n(x_0)}{\varepsilon} \right| \\
    &= \lim_{n\to\infty}\frac{1}{n} \ln \left| \frac{\diff f^n(x_0)}{\diff x_0} \right| \\
    &= \lim_{n\to\infty}\frac{1}{n} \ln \left| \prod_{i=0}^{n-1} f'(x_i) \right|
  \end{align*}
  Dieser ist ein Maß für die Fehlerverstärkung (Instabilität). Diese
  findet statt bei $\lambda > 0$.
\end{theorem}

\begin{theorem}[Definition]
  Die \acct{invariante Dichte} bestimmt die Dichte einer Iterierten
  für eine Abbildung $x_{n+1} = f(x_n)$.
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx,xscale=3,yscale=2]
      \draw[<->] (0,1.5) node[right] {$N$} |- (1.5,0) node[below] {$x$};
      \draw[MidnightBlue] plot[const plot] coordinates {
        (0,.7) (.1,.5) (.2,.3) (.3,.2) (.4,.4) (.5,.2) (.6,.6) (.7,.7) (.8,.8) (.9,.9) (1,1)
      };
    \end{tikzpicture}
    \caption{Histogramm einer Trajektorie.}
    \label{fig:3.2}
  \end{figure}
  Betrachte dazu ein Histogramm, wie es etwa in
  Abbildung~\ref{fig:3.2} zu sehen ist, für eine \enquote{typische}
  (nicht periodische, ergodische) Trajektorie. Der Limes für
  $\Delta x \to 0$ und $N \to \infty$ liefert die invariante Dichte
  $\varrho(x)$ mit der Normierung $\int \varrho(x) \diff x = 1$.  Das
  \acct{invariante Maß} oder die invariante Dichte ist definiert als
  \begin{align*}
    \varrho_{x_0}(x)
    &= \lim_{N\to\infty} \frac{1}{N} \sum_{i=0}^{N} \delta(x-f^i(x_0)) \\
    \varrho(x)
    &= \int \diff y\ \varrho(y)\,\delta(x-f(y))
  \end{align*}
  Hängt $\varrho$ nicht von $x_0$ ab, so heißt das System ergodisch.
\end{theorem}

\subsection{Logistische Abbildung}
\index{Logistische Abbildung}
Die Vorschrift lautet
\begin{equation*}
  x_{n+1} = f_r(x_n) = r x_n (1-x_n)
\end{equation*}
mit dem Kontrollparameter $r$ unter der Einschränkung $0 < r \leq 4$.
Die nullte Iterierte der logistischen Abbildung ist in
Abbildung~\ref{fig:3.3} zu sehen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,xscale=3,yscale=2]
    \draw[<->] (0,1.5) node[right] {$f(x_n)$} |- (1.5,0) node[below] {$x_n$};
    \draw[MidnightBlue] plot[domain=0:1,samples=100] (\x,{4*\x*(1-\x)});
    \foreach \i in {{1/2},1} {
      \draw (\i,2pt) -- (\i,-2pt) node[below] {$\i$};
    }
  \end{tikzpicture}  
  \caption{Graph der logistischen Abbildung.}
  \label{fig:3.3}
\end{figure}

Dies ist ein mathematisches Modell für
\begin{itemize}
\item den gedämpften, getriebenen Oszillator
\item Populationsdynamik in einem abgeschlossenen Lebensraum.
\end{itemize}

Welche Eigenschaften hat die logistische Abbildung? Dazu diskutieren
wir die Fixpunkte $f_r(x_0) = x_0$ und die periodischen $n$-Zyklen
$f_r^n(x_0) = x_0$.
\begin{enumerate}
\item Die Fixpunkte (also die $1$-Zyklen) erhalten wir als Lösungen
  der Gleichung
  \begin{equation*}
    r x_0 (1-x_0) = x_0
    \implies x_0 = 0
    \quad\text{oder}\quad x_0 = 1 - \frac{1}{r} > 0
    \quad\text{für}\quad r>1
  \end{equation*}
  Der Fixpunkt ist stabil, falls $|f_r'(x_0)| = |r (1-2x_0)| < 1$,
  sonst ist er instabil. Da $|f_r'(x_0=0)|=r$ und damit $x_0=0$
  handelt es sich hier um einen stabilen Fixpunkt für $0<r<1$ und
  einen instabilen Fixpunkt für $r > 1$. Mit $|f_r'(x_0 = 1 -
  1/r)|=|2-r|$ folgt $x_0 = 1- 1/r$, was stabil ist für $1 < r < r_1 =
  3$ und instabil für $r > 3$.
\item Die Fixpunkte des $2$-Zyklus sind $f_r(f_r(x_0))=x_0$, also
  \begin{align*}
    x_1 &= r x_0 (1-x_0) \\
    x_2 &= r x_1 (1-x_1) = r^2 x_0 (1-x_0) (1-rx_0(1-x_0)) = x_0
  \end{align*}
  Wir müssen die Lösungen $x_0$ von $r^3x^4
  -2r^3x^3+r^2(r+1)x^2+(1-r^2)x=0$ finden. Diese Lösungen sind
  \begin{align*}
    x_1 = 0 \;,\quad
    x_2 = 1 - \frac{1}{r} \;,\quad
    x_{3,4} = \frac{1}{2} + \frac{1}{2r}
    \pm \sqrt{\left(\frac{1}{2} + \frac{1}{2r}\right)^2 - \frac{r+1}{r^2}}
  \end{align*}
  Was passiert bei $r = r_1 = 3$? Es findet eine Periodenverdopplung
  statt. Der $1$-Zyklus wird instabil und ein stabiler $2$-Zyklus
  entsteht. Dies drückt sich in einer \acct{Heugabelbifurkation} aus.
\end{enumerate}

Eine Fortsetzung dieses Schema zeigt, dass eine weitere
Periodenverdopplung bei $r = r_2 \approx \num{3.45}$
stattfindet. Dieses Mal wird der $2$-Zyklus instabil und ein stabiler
$4$-Zyklus entsteht. Die sich daraus ergebende Verallgemeinerung ist:
\begin{enumerate}
\item Für $r_{n-1} < r < r_n$ existiert ein stabiler $2^{n-1}$-Zyklus
  mit den Elementen $x_0,x_1,\ldots,x_{2^{n-1}-1}$ und es gilt
  \begin{equation*}
    f_r(x_i) = x_{i+1} \;,\quad
    f_r^{2^{n-1}}(x_i) = x_i \;,\quad
    \left|\frac{\diff}{\diff x_0} f_r^{2^{n-1}}(x_0)\right|
    = \biggl|\prod_i f_r'(x_i)\biggr| < 1
  \end{equation*}
\item Bei $r=r_n$ werden alle Punkte des $2^{n-1}$-Zyklus gleichzeitig
  durch eine Gabelbifurkation instabil. Die Bifurkation tritt in
  $f_r^{2^n} = f_r^{2^{n-1}} \circ f_r^{2^{n-1}}$ auf und führt zu
  einem stabilen $2^n$-Zyklus im Bereich $r_n < r < r_{n+1}$.
\end{enumerate}

\begin{notice}[Zusammenfassung der numerischen Resultate:]
  \begin{enumerate}
  \item Es gibt einen periodischen Bereich
    ($r < r_\infty = \lim_{n\to\infty} r_n$). In diesem Bereich ist der
    Liapunov-Exponent stets kleiner als Null. Die $r_n$ skalieren wie
    $r_n = r_\infty - c \delta^{-n}$ für $n \gg 1$ mit
    \[ r_\infty \approx \num{3.569945} \;,\quad \delta =
    \num{4.6692016091}\;. \]
    Das $\delta$ hat auch den Namen \acct{Feigenbaumkonstante}.

    Die Abstände $d_n$ zwischen $x = 1/2$ und dem nächstliegenden
    Punkt in einem $2^n$-Zyklus haben konstante Verhältnisse:
    \begin{equation*}
      \frac{d_n}{d_{n+1}} = -\alpha
      \quad\text{für}\quad n \gg 1
      \quad\text{mit}\quad \alpha = \num{2.5029078750}
    \end{equation*}
    Das $\alpha$ ist eine weitere Feigenbaumkonstante.
  \item Chaotischer Bereich ($r > r_\infty$). Hier ist der
    Liapunov-Exponent meist größer als Null. Die chaotischen Bereiche
    wachsen durch inverse Bifurkationen zusammen bis bei $r = 4$ die
    Iterationen über das ganze Intervall $[0,1)$ verteilt sind. Die
    $r$-Fenster, in denen kein Chaos auftritt ($\lambda < 0$) werden
    durch periodische $p$-Zyklen charakterisiert ($p = 3,5,6,\ldots$),
    bei denen sukzessive Bifurkationen auftreten:
    \begin{gather*}
      p, 2p, 2^2p, \ldots \quad\text{(Verdopplungen)}
      \intertext{aber auch}
      p, 3p, 3^2p, \ldots \quad\text{(Verdreifachungen)}
    \end{gather*}
    außerdem Vervierfachungen $4^n p$, usw.
  \end{enumerate}
\end{notice}

\begin{theorem}[Definition]
  Ein $2^n$-Superzyklus (superstabiler Zyklus) ist definiert durch
  \begin{equation*}
    \frac{\diff}{\diff x_0} f_{R_n}^{2^n}(x_0) = \prod_i f_{R_n}'(x_i) = 0
    \implies \boxed{\lambda \to -\infty}\;.
  \end{equation*}
\end{theorem}

Bei der logistischen Abbildung enthält der Superzyklus immer den Wert
$x_0=1/2$ ($f'(x) = 0$ gilt nur bei $x=1/2$). Die Abstände $d_n$ sind
definiert durch
\begin{equation*}
  d_n = f_{R_n}^{2^{n-1}}\left(\frac{1}{2}\right) - \frac{1}{2}
\end{equation*}
bei Superzyklus $r = R_n$. Die Superzyklen und Abstände $d_n$ sind
hierbei in Abbildung~\ref{fig:30.10.14-0} schematisch visualisiert.
\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = 0.4\textwidth]{feigenbaum.png}};
  \end{tikzpicture}
  \caption{Die Abstände $d_n$ der am dichtesten bei $x=1/2$ liegenden
    Fixpunkte für superstabile $2^N$-Zyklen (schematisch). Von
    \cite{schuster}.}
  \label{fig:30.10.14-0}
\end{figure}


Der erste Superzyklus tritt auf bei $R_0 = 2$:
\begin{equation*}
  f_{R_0}(x) = 2 x (1-x)
\end{equation*}
Eine kleine Störung $x = 1/2 + \varepsilon$ führt zu
\begin{equation*}
  f_{R_0}(x) = (1+2\varepsilon)\left(\frac{1}{2}-\varepsilon\right)
  = \frac{1}{2} - 2 \varepsilon^2 .
\end{equation*}
Das Konvergenzverhalten lässt sich nicht mehr durch einen
Liapunovexponent beschreiben, da $\lambda \to - \infty$. Man spricht
von \acct{Superkonvergenz}. Weitere Superzyklen findet man bei $R_1 = 1 + \sqrt{5} = \num{3.236}$ und $R_2 = \num{3.4985}$. Die $R_n$ skalieren wie die $r_n$ der Bifurkationspunkte mit einem etwas anderen Vorfaktor aber sie haben denselben Grenzwert $r_\infty$:
\begin{equation*}
  R_n = r_\infty - c' \delta^{-n}
\end{equation*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: