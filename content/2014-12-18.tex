\minisec{Abstecher: Der goldene Schnitt}

Jede irrationale Zahl lässt sich durch eine Folge von endlichen
Kettenbrüchen (also eine Folge rationaler Zahlen) approximieren. Als
\acct[goldener Schnitt]{goldenen Schnitt} wird diejenige irrationale
Zahl bezeichnet, die sich am schlechtesten durch rationale Zahlen
approximieren lässt.
\begin{equation}
  \label{eq:10.1}
  w^* \equiv \cfrac{1}{1+\cfrac{1}{1+\cfrac{1}{1+\cdots}}}
\end{equation}
Die sogenannten \acct{Fibonacci-Zahlen} $F_n$, die durch $F_{n+1} =
F_n + F_{n-1}$, $F_0 = F_1 = 1$ definiert sind, liefern über
\begin{equation*}
  w_n = \frac{F_n}{F_{n+1}} = \frac{F_n}{F_n + F_{n-1}}
  = \cfrac{1}{1+\cfrac{F_{n-1}}{F_n}}
  = \underbrace{\cfrac{1}{1+\cfrac{1}{1+\ldots}}}_{\text{$n$ mal}}
\end{equation*}
eine Folge rationaler Zahlen, die gegen $w^* = \lim_{n\to\infty} w_n$
konvergieren. Im Limes $n\to\infty$ folgt aus \eqref{eq:10.1}
\begin{align*}
  w^* = \frac{1}{1+w^*} \implies w^* (1+\omega^*) = 1
  \intertext{also ist $w^*$ Lösung der Gleichung $w^2 + w - 1 = 0$}
  w^* = \frac12 (\sqrt{5} - 1) = \num{0.6180331}
\end{align*}

\begin{notice}[Geometrische Interpretation:]
  Aufteilung einer Linie der Länge $L$, sodass
  \begin{equation*}
    w^* = \frac{\ell}{L} = \frac{L-\ell}{\ell}
  \end{equation*}
  \begin{center}
    \begin{tikzpicture}[x=3cm]
      \draw (0,0) -- node[above,pos=.3] {$\ell$} node[above,pos=.8] {$L-\ell$} (1,0);
      \draw (0,2pt) -- (0,-2pt) node [below] {$0$};
      \draw (1,2pt) -- (1,-2pt) node [below] {$L$};
      \draw (0.6180331,2pt) -- (0.6180331,-2pt) node [below] {$w^*$};
    \end{tikzpicture}
  \end{center}
\end{notice}

Lokale Universalität: Der Übergang von Quasiperiodizität zum Chaos bei
einer speziellen irrationalen Windungszahl $w = w^* =
\lim_{n\to\infty} w_n = \lim_{n\to\infty} F_n/F_{n+1}$ erfordert eine
Anpassung von $\Omega$ und $k$, d.h.\ für jedes $k$ ist $\Omega(k)$ so
zu wählen, dass sich die Windungszahl $w^*$ ergibt.

Numerische Resultate für die Kreisabbildung (Shenker, 1982):
\begin{enumerate}[(a)]
\item\label{item:10.a} Die Parameter $\Omega_n(k)$, welche die
  Windungszahlen $w_n$ erzeugen, bilden eine geometrische Folge, die
  sich einer Konstanten nähert gemäß
  \begin{equation*}
    \Omega_n(k) = \Omega_\infty(k) - \const \tilde\delta^{-n}
    \quad\text{wobei}\quad
    \tilde\delta =
    \begin{cases}
      \num{-2.6180339} = -(w^*)^{-1} & \text{für $|k|<1$} \\
      \num{-2.83362} & \text{für $|k|=1$} \\
    \end{cases}
  \end{equation*}
  eine universelle Konstante ist, die jedoch von $w^*$ abhängt.
\item\label{item:10.b} Die Abstände $d_n$ von $\theta=0$ zum nächsten Element eines
  Zyklus, der zu $w_n$ gehört
  \begin{equation*}
    d_n = f_{\Omega_n}^{F_n}(0) - F_{n-1}
  \end{equation*}
  haben das Skalenverhalten
  \begin{equation*}
    \lim_{n\to\infty} \frac{d_n}{d_{n+1}} = \tilde\alpha
    \quad\text{mit}\quad
    \tilde\alpha =
    \begin{cases}
      \num{-1.61803} = -(w^*)^2 & \text{für $|k|<1$} \\
      \num{-1.28857} & \text{für $|k|=1$} \\
    \end{cases}
  \end{equation*}
  als universelle Konstante.
\item Die periodische Funktion
  \begin{equation*}
    \begin{split}
      \mu(t_j)
      &= \Theta^n(t_j) - t_j \;,\quad j=0,1,2,\ldots \\
      &\equiv \Theta(j\cdot w_n) \equiv f^j(0)
    \end{split}
  \end{equation*}
  variiert für $|k|<1$ und $\Omega_n\to\Omega_\infty$ stetig mit
  $t$. Für $|k|=1$ wird die Folge unstetig. Dies deutet den Übergang
  von Quasiperiodizität zum Chaos an.
\item Das Leistungsspektrum
  \begin{equation*}
    A(\omega) = \frac{1}{F_{n+1}} \sum_{j=0}^{F_{n+1}-1} \mu(t_j) \ee^{2\pi\ii\omega t_j}
  \end{equation*}
  für $|k|=1$ und $\omega$ zeigt im Limes $n\to\infty$ selbstähnliche
  Strukturen, d.h.\ die Hauptstrukturen zwischen zwei aufeinander
  folgenden Maxima sind im wesentlichen die Gleichen.
\end{enumerate}

Zu (\ref{item:10.b}) Ableitung der zugehörigen Funktionalgleichung.
Definiere dazu
\begin{equation*}
  f_n(x) \equiv \tilde\alpha^n f^{(n)}(\tilde\alpha^{-n} x)
  \quad\text{mit } f^{(n)}(x) \equiv f^{F_{n+1}}(x) - F_n
\end{equation*}
Beachte $f_{k,\Omega}^q(0)=p$ für Windungszahl $w=p/q$. Das Skalenverhalten
\begin{equation*}
  \lim_{n\to\infty} \frac{d_n}{d_{n+1}} = \tilde\alpha
  \quad\text{mit}\quad
  d_n = f_{\Omega_n}^{F_n}(0) - F_{n-1}
\end{equation*}
lässt sich schreiben als
\begin{equation*}
  \lim_{n\to\infty} \tilde\alpha^n d_n
  \sim \lim_{n\to\infty} \tilde\alpha^n f^{(n)}(0)
  = \lim_{n\to\infty} f_n(0) = \const
\end{equation*}
Analog zu Periodenverdopplung: $\{f_n(x)\}$ konvergiert gegen eine
universelle Funktion
\begin{align*}
  \lim_{n\to\infty} f_n(x) &= f^*(x)
  \quad\text{mit}\quad
  f^*(x)
  \text{ Lösung einer Fixpunktgleichung} \\
  f^{(n+1)}(x) &= f^{F_{n+2}}(x) - F_{n+1}
  = f^{F_{n+1}}[f^{F_n}(x)] - (F_n+F_{n-1}) \\
  &= f^{F_{n+1}} [f^{(n-1)}(x) + F_{n-1}] - (F_n+F_{n-1}) \\
  &= f^{F_{n+1}} [f^{(n-1)}(x)] - F_n
  \quad\text{(beachte $f(x+1)=f(x)+1$)} \\
  &= f^{(n)} [f^{(n-1)}(x)] = f^{(n-1)}[f^{(n)}(x)] \\
  f_{n+1}(x) &= \tilde\alpha^{n+1} f^{(n+1)}(\tilde\alpha^{-(n+1)} x) \\
  &= \tilde\alpha^{n+1} f^{(n)}[f^{(n-1)}(\tilde\alpha^{-(n+1)}x)] \\
  &= \tilde\alpha^{n+1} f^{(n)}[\tilde\alpha^{-(n-1)}f_{n-1}(\tilde\alpha^{-2}x)] \\
  &= \tilde\alpha f_n[\tilde\alpha f_{n-1}(\tilde\alpha^{-2}x)]
  \intertext{Grenzfall $n\to\infty$: $f^*(x)$ ist Lösung der Fixpunktgleichung}
  \Aboxed{ f^*(x) &= \tilde\alpha f^*[\tilde\alpha f^*(\tilde\alpha^{-2} x)] }
\end{align*}
Ansatz für $|k|<1$: $\bar f^*(x) = x-1$ ist Lösung. Einsetzen:
\begin{align*}
  x-1 &= \tilde\alpha \bar f^*\left[\tilde\alpha\left(\frac{x}{\tilde\alpha^2}-1\right)\right] = x - \tilde\alpha^2 - \tilde\alpha \\
  \implies \tilde\alpha^2 + \tilde\alpha - 1 &= 0 \\
  \implies \tilde\alpha &= \begin{cases} - w^{\ast}-1 = -(w^{\ast})^{-1}, \\ w^{\ast} \end{cases} \quad \text{ für $|k|<1$}
\end{align*}
wobei wir $w^{\ast} = 1/(1+w^{\ast})$ verwendet haben. Für $k=1$:
\begin{equation*}
  f(\varepsilon) = \varepsilon + \Omega - \frac{1}{2\pi}
  \underbrace{\sin(2\pi\varepsilon)}_{\approx 2\pi\varepsilon-\frac16(2\pi\varepsilon)^3}
  \approx \Omega - \frac{2\pi^2}{3} \varepsilon^3
\end{equation*}
der lineare Term in $\varepsilon$ verschwindet.

Der Ansatz $f^*(x) = 1 + a x^3 + b x^6 + \ldots$ liefert nach
einsetzen $\tilde\alpha = \num{-1.28857}\ldots$ für $k=1$.

Zu (\ref{item:10.a}), in Analogie zur Periodenverdopplung, sind die
$\tilde\delta$-Konstanten Eigenwerte der linearen
Fixpunkt-Gleichung. Die Lösung ist komplizierter, da die
Rekursionsgleichungen von zweiter Ordnung sind, d.h.\ von $f_n$ und
$f_{n+1}$ abhängen.

\subsubsection{Globale Universalität}

Skalenverhalten für diejenigen $\Omega$-Werte, bei denen keine
Modenkopplung auftritt (Bereich komplementär zu den Arnold-Zungen).
Numerische Resultate (M.\,H.\ Jensen, P. Beck, T. Bohr,1984):
\begin{itemize}
\item Für $k\to1$ strebt die Gesamtlänge der Stufen in der
  Teufelstreppe mit einem Potenzgesetz nach $1$.
  \begin{equation*}
    C = 1 - \sum_{p,q} \Delta\Omega\left(\frac{p}{q},k\right)
    \sim (1-k)^\beta \quad\text{mit $\beta=\num{0.34}$}
  \end{equation*}
  universeller Exponent für alle $f(\Theta)$ mit einem kubischen
  Sattelpunkt bei $k=1$.
\item Bei $k=1$ bilden die $\Omega$-Werte, die zu irrationalen
  Windungszahlen gehören, eine selbstähnliche Cantormenge (vom Maß
  $0$) mit universeller Hausdorff-Dimension $D^* = \num{0.87}$.
\end{itemize}

Frage: Lassen sich die für die Kreisabbildung diskutierten
universellen Eigenschaften wirklich an realen physikalischen Systeme
beobachten?

Kriterien für Systeme, das sich analog zur Kreisabbildung verhält:
\begin{itemize}
\item Leistungsspektrum zeigt zwei oder drei inkommensurable
  Frequenzen vor dem Einsetzen von breitbandigem Rauschen. Für
  geeigneten Schnitt im Phasenraum findet man $\Theta_{n+1} =
  f(\Theta_n)$ wobei $f(\Theta+1) = f(\Theta) + 1$ mit $f(\Theta)$
  qualitativ wie Kreisabbildung.
\item Analyse der Zeitreihe der $\Theta_n$ zeigt universelle Züge in
  der Nähe des Übergangs zum Chaos:
  \begin{itemize}
  \item Die Teufelstreppe der Intervalle mit Modenkopplung wird durch
    einen Farey-Baum angeordnet.
  \item Die Bereiche ohne Modenkopplung bilden ein Fraktal mit
    Hausdorff-Dimension $D=\num{0.87}$.
  \item Nichttriviales Skalenverhalten mit $\tilde\alpha =
    \num{-1.289}$ für Windungszahl $w^* = (\sqrt{5} - 1)/2$.
  \end{itemize}
\end{itemize}

Experimente mit dynamischem Verhalten der Kreisabbildung:
\begin{itemize}
\item Das getriebene Pendel:
  \begin{equation*}
    \ddot\Theta + \gamma \dot\Theta + \sin\Theta = A \cos\omega t + B
  \end{equation*}
  und $t_n = n \tau$ mit $\tau = 2\pi/\omega$
  \begin{equation*}
    \Theta_n = \Theta(t_n) = \Theta(n\tau)
  \end{equation*}
\item Elektrische Leitfähigkeit von Barium-Natrium-Niobat
\item Das dynamische Verhalten von Herzzellen
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: