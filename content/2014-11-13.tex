\paragraph{Der Feigenbaumattraktor}

Bisher haben wir betrachtet
\begin{equation*}
  r < r_\infty = \lim_{n\to\infty} R_n = \num{3.5699456}
\end{equation*}
Die Folge $x_i = f(x_{i-1})$ nähert sich einem periodischen
$2^n$-Zyklus (periodische Bahn). Was passiert beim Erreichen des
Wertes $r=r_\infty$? Die Bahnlänge geht gegen unendlich, also ist die
Folge nicht mehr periodisch. Die Folge $x_i = f_{r_\infty}(x_{i-1})$
läuft gegen einen \acct{Attraktor}.
\begin{notice}[Hinweis:]
  Der Liapunov-Exponent $\lambda$ wird $\lambda = 0$ für $r =
  r_\infty$. Der Feigenbaumattraktor ist kein \enquote{seltsamer
    Attraktor}.
\end{notice}
Der Feigenbaumattraktor zeigt Selbstähnlichkeit.

Zur Konstruktion:
\begin{description}
\item[$r<r_1$:] Ein Fixpunkt im Intervall $[0,2/3]$.
\item[$r_1<r<r_2$:] Zwei Fixpunkte in zwei Intervallen.
\item[$r_n<r<r_{n+1}$:] $2^n$ Fixpunkte in $2^n$ Intervallen.
\end{description}
Im Limes $n\to\infty$ bildet der Feigenbaumattraktor ein Fraktal.

\paragraph{Exkurs}

Betrachten wir die Begriffe \acct{Fraktal} und \acct{fraktale
  Dimension} (Hausdorffdimension) einmal näher. Fraktale sind Gebilde,
die im Allgemeinen keine ganzzahlige sondern eine gebrochene Dimension
(Hausdorff-Dimension) besitzen und zudem einen hohen Grad von
Selbstähnlichkeit und Skaleninvarianz aufweisen.

Die Dimension einer Menge ist z.B.\
\begin{itemize}
\item Punktmenge: $D=0$,
\item Linie: $D=1$,
\item Fläche: $D=2$,
\item Volumen: $D=3$.
\end{itemize}
Auch wenn sie intuitiv wirken stellt sich die Frage, wie man auf
diese Werte kommt. Betrachte eine Punktmenge in $d$ Dimensionen und
überdecke sie mit $d$-dimensionalen Kugeln mit Durchmesser
$\varepsilon$. Sei $N(\varepsilon)$ die Anzahl der zur Überdeckung
notwendigen Kugeln. Es gilt:
\begin{equation*}
  N(\varepsilon) \sim \varepsilon^{-D} \quad\text{für } \varepsilon \to 0
\end{equation*}

\begin{theorem}[Definition]
  Die Größe
  \begin{equation*}
    D = - \lim_{\varepsilon\to0} \frac{\ln N(\varepsilon)}{\ln \varepsilon}
  \end{equation*}
  heißt \acct{Hausdorffdimension} der Menge.
\end{theorem}

\begin{example}
  \begin{enumerate}
  \item Die Cantormenge (Abbildung~\ref{fig:13.11.14-0}).
    \begin{figure}[tb]
      \centering
      \begin{tikzpicture}[gfx,x=5cm]
        \draw (0,0) -- node[above] {$\ell_0=1$} (1,0) node[right] {$n=0$, $\ell_0=1$};
        \draw (0,-1) -- (1/3,-1) (2/3,-1) -- (1,-1) node[right] {$n=1$, $\ell_0=2/3$};
        \draw (0,-2) -- (1/9,-2) (2/9,-2) -- (3/9,-2) (6/9,-2) -- (7/9,-2) (8/9,-2) -- (1,-2) node[right] {$n=2$, $\ell_0=4/9$};
      \end{tikzpicture}
      \caption{Abbildungsvorschift der Cantormenge: Mit jeder
        Iteration wird aus dem Intervall das mittlere Drittel
        herausgenommen.}
      \label{fig:13.11.14-0}
    \end{figure}
    Die Überdeckung in Stufe $n$ ergibt sich aus $2^n$ Intervallen mit
    der Länge $\varepsilon_n = (1/3)^n$.
    \begin{align*}
      N(\varepsilon_n) &= 2^n \;,\quad \varepsilon_n = \left(\frac13\right)^n \\
      \implies D
      &= - \lim_{n\to\infty}\frac{\ln N(\varepsilon_n)}{\ln \varepsilon_n} \\
      &= - \lim_{n\to\infty}\frac{n \ln 2}{n \ln 1/3} \\
      &= \frac{\ln 2}{\ln 3} \approx \num{0.6309}
    \end{align*}
  \item Kochsche Kurve (Abbildung~\ref{fig:13.11.14-1})
    \begin{figure}[tb]
      \centering
      \begin{tikzpicture}[gfx,scale=1.5,decoration=Koch snowflake]
        \draw[MidnightBlue] { (0,0) -- ++(60:1) -- ++(-60:1) -- cycle };
        \draw[MidnightBlue] decorate { (2,0) -- ++(60:1) -- ++(-60:1) -- cycle };
        \draw[MidnightBlue] decorate { decorate { (4,0) -- ++(60:1) -- ++(-60:1) -- cycle } };
        \node[below] at (0.5,-.5) {$\ell_0 = 3$};
        \node[below] at (2.5,-.5) {$\ell_0 = 3 \cdot \frac43 = 4$};
      \end{tikzpicture}  
      \caption{Die Kochsche Schneeflocke.}
      \label{fig:13.11.14-1}
    \end{figure}
    Für die $n$-te Iteration gilt
    \begin{equation*}
      \ell_n = 3 \left( \frac{4}{3} \right)^{n-1}.
    \end{equation*}
    Die Überdeckung in Stufe $n$ ist durch $3 \cdot 4^n$ Intervalle
    der Länge $\varepsilon_n = (1/3)^n$.
    \begin{align*}
      N(\varepsilon_n) &= 3 \cdot 4^n \;,\quad \varepsilon_n = \left(\frac13\right)^n \\
      \implies D
      &= - \lim_{n\to\infty}\frac{\ln(3\cdot 4^n)}{n \ln 1/3} \\
      &= \frac{\ln 4}{\ln 3} \approx \num{1.2619}      
    \end{align*}
  \end{enumerate}
\end{example}

Zurück zum Feigenbaumattraktor: In der Stufe $n$, also für $r_n < r <
r_{n+1}$ finden wir $2^n$ Fixpunkte in $2^n$ Intervallen. Nehmen wir an, dass die Intervalle wie die $d_n$ skalieren:
\begin{align*}
  \frac{d_n}{d_{n+1}} &= -\alpha \\
  \implies \varepsilon_n &\sim \frac{1}{\alpha^n} \;,\quad N(\varepsilon_n) = 2^n \\
  \implies D &\simeq - \lim_{n\to\infty} \frac{\ln N(\varepsilon_n)}{\ln\varepsilon_n} = \frac{\ln 2}{\ln \alpha} = \num{0.7555}
\end{align*}
Genauer finden wir
\begin{align*}
  \varepsilon_{n+1} &\approx \left[\frac12 \left(\frac12 + \frac1{\alpha^2}\right)\right] \varepsilon_n \\
  \implies D &\simeq - \frac{\ln 2}{\ln \left[\frac12 \left(\frac12 + \frac1{\alpha^2}\right)\right]} = \num{0.5439}
\end{align*}
Der Feigenbaumattraktor ist ein Fraktal mit Hausdorffdimension $D =
\num{0.548}$. Der Feigenbaumattraktor ist ein \enquote{dünnes
  Fraktal}, d.h.\ das Fraktal hat das Maß $\mu = 0$ (Nullmenge).


Für den Bereich $r > r_\infty$ herrscht sensitive
Parameterabhängigkeit. Das bedeutet, dass periodische und chaotische
Gebiete eng miteinander verwoben sind. Parameterbereiche mit
Liapunov-Exponent $\lambda > 0$ bilden ein \enquote{fettes Fraktal},
d.h.\ das Fraktal hat ein Maß $\mu > 0$ und die Hausdorffdimension
$D = 1$.

\begin{example}
  Beispiel für ein \enquote{fettes Fraktal} (Abbildung~\ref{fig:13.11.14-2}):
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx,x=5cm]
      \draw (0,0) -- node[above] {$\ell_0=1$} (1,0) node[right] {$n=0$, $\ell_0=1$};
      \draw (0,-1) -- (1/3,-1) (2/3,-1) -- (1,-1) node[right] {$n=1$, $\ell_0=2/3$};
      \draw (0,-2) -- ({.5*(1/3-1/18)},-2) ({.5*(1/3+1/18)},-2) -- (3/9,-2) (6/9,-2) -- ({6/9+.5*(1-6/9-1/18)},-2) ({6/9+.5*(1-6/9+1/18)},-2) -- (1,-2) node[right] {$n=2$, $\ell_0=(1-1/9)\ell_1$};
      \draw[dotted,DarkOrange3] (1/3,-1) -- node[above] {$1/3$} (2/3,-1);
      \draw[dotted,DarkOrange3] ({.5*(1/3-1/18)},-2) -- node[above] {$1/9$} ({.5*(1/3+1/18)},-2);
    \end{tikzpicture}    
    \caption{Modifizierte Cantor-Menge.}
    \label{fig:13.11.14-2}
  \end{figure}
  \begin{align*}
    \ell_n &= \left( 1 - \frac{1}{3^n} \right) \ell_{n-1} \\
    \varepsilon_n &= \frac{1}{2} \left( 1 - \frac{1}{3^n} \right) \varepsilon_{n-1}
  \end{align*}
  Für die Überdeckung in Stufe $n$ haben wir $2^n$ Intervalle der
  Länge $\varepsilon_n$.
  \begin{align*}
    D &= - \lim_{n\to\infty} \frac{\ln N(\varepsilon)}{\ln \varepsilon_n} = - \lim_{n\to\infty} \frac{n \ln 2}{n \ln 1/2} = 1 \\
    \mu &= \lim_{n\to\infty} \ell_n = \prod_{n=1}^{\infty} \left( 1 - \frac{1}{3^n} \right) = \num{0.5601} > 0
  \end{align*}
\end{example}

Frage: Wie entstehen die stabilen $p$-Zyklen im chaotischen Bereich?
Tangentenbifurkation.
\begin{itemize}
\item $p$-Zyklen entstehen \enquote{aus dem Nichts}.
\item Jeder $p$-Zyklus durchläuft nach der Entstehung ein
  vollständiges Feigenbaumszenario mit Periodenverdopplungen bei $r^{(p)} =
  r_n^{(p)}$.
  \begin{equation*}
    r_n^{(p)} = r_\infty - c \delta^{-n}
  \end{equation*}
  Superzyklen bei $r = R_n^{(p)}$.
  \begin{align*}
    R_n &= r_\infty - c' \delta^{-n} \\
    \frac{d_n}{d_{n+1}} &= - \alpha \qquad \lambda \le 0
  \end{align*}
\end{itemize}

\paragraph{Logistische Abbildung bei $r = 4$}
\begin{equation*}
  f_4(x) = 4 x (1-x) \;,\quad f_4:[0,1]\to[0,1]
\end{equation*}
zeigt ergodisches Verhalten (kein Attraktor). Man kann sich dies
klarmachen, wenn man substituiert:
\begin{equation*}
  x_n = \frac{1}{2} [ 1 - \cos(2 \pi y_n) ] \equiv h(y_n)
\end{equation*}
Damit erhält man
\begin{align*}
  x_{n+1} &= \frac{1}{2} [ 1 - \cos(2 \pi y_{n+1}) ] = 4 x_n (1-x_n) = [1 - \cos(2 \pi y_n)] [1+\cos(2 \pi y_n)] \\
  &= \frac{1}{2} [1 - \cos({\color{DarkOrange3} 4 \pi y_n})] = \frac{1}{2} [1 - \cos({\color{DarkOrange3} 2 \pi y_{n+1}})] \\
  \implies y_{n+1} &= 2 y_n \bmod 1
\end{align*}
Dies ist der bekannte Bernoulli-Shift. Dieser ist eine ergodische
Folge in $[0,1]$. Die invariante Dichte ist konstant
\begin{equation*}
  \varrho(y) = 1
\end{equation*}
für die Bernoulli-Abbildung. Die invariante Dichte der Logistischen
Abbildung berechnet sich per Definition aus
\begin{align*}
  \varrho(x) &= \lim_{N\to\infty} \frac{1}{N} \sum_{n=0}^{N-1} \delta(x-\underbrace{x_n}_{h(x)}) \\
  &= \int_0^1 \diff y\ \underbrace{\varrho(y)}_{=1} \, \underbrace{\delta(x - h(y))}_{\sum_n \frac{1}{|h'(y_n)|}}
  \intertext{mit den Lösungen $y_n$ der Gleichung $\cos(2 \pi y_n) = 1 - 2 x$}
  &= \frac{1}{\pi} \frac{2}{|\sin(2 \pi y_1)|} \quad\text{mit } y_1 = \frac{1}{2\pi}\arccos(1-2x) \\
  &= \frac{1}{\pi} \frac{1}{\sqrt{x(1-x)}}
\end{align*}
Somit zeigt sich, dass eine konstante invariante Dichte, wie sie etwa
die Bernoulli-Abbildung besitzt, nicht zwangsweise mit chaotischen
verhalten einhergeht.

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: