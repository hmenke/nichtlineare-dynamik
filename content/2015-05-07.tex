\subsubsection{Verallgemeinerung auf mehrdimensionale integrable Systeme }

Wir verwenden den Ansatz
\begin{align*}
  \psi(\bm{q}) &= A(\bm{q}) \ee^{\ii S(\bm{q})/\hbar}.
\end{align*}
Dieser führt auf die Hamilton-Jacobi-Differentialgleichung für
$\hbar \to 0$
\begin{align*}
  H(\bm{q},\nabla_{\bm{q}}S) - E &= 0, \quad \bm{p} = \nabla_{\bm{q}}S(\bm{q}).
\end{align*}
Die Lösung $S(\bm{q})$ kann jedoch nur bis zu den Singularitäten
\begin{align*}
  \det \left| \frac{\partial p_j}{\partial q_k} \right| \to \infty
\end{align*}
verwendet werden. Analoges Vorgehen erfolgt mit
\begin{align*}
  \overline{S}(\bm{p}) &= S(\bm{q}) - \bm{p} \cdot \bm{q} \\
  \implies H(-\nabla_{\bm{p}}\overline{S},\bm{p}) - E &= 0, \quad \bm{q} = -\nabla_{\bm{p}}\overline{S}(\bm{p}).
\end{align*}
Im Gegensatz zum eindimensionalen Fall gibt es keine Garantie, dass
fortwährendes Überschreiten von Kaustiken in nicht-separablen Systemen
zu einer geschlossenen $N$-dimensionalen Fläche ohne Kanten führt.

Im Folgenden nehmen wir ein separables und integrables System an,
d.h.\ die Hamilton-Jacobi-Differentialgleichung hat globale
Lösungen. Somit ist die $N$-dimensionale Fläche ein invarianter Torus.

\minisec{Definition des Maslov-Index} 

Auf dem $\bm{q}$-Zweig schreiben wir $\sigma_{\bm{q}}(\bm{x})$, auf
dem $\bm{p}$-Zweig gilt
\begin{align*}
  \sigma_{\bm{p}} &= \sigma_{\bm{q}}(\bm{x}) - \mathrm{sgn}\left( \frac{\partial p_\ell}{\partial q_m} \right), \\
  \mathrm{sgn}\left( \frac{\partial p_\ell}{\partial q_m} \right) &\equiv \sum\limits_{i}^N \sign(\lambda_i),
\end{align*}
wobei $\lambda_i$ die Eigenwerte der Matrix
$\partial p_\ell / \partial q_m$ sind. Es gibt $N$ unabhängige Wege
$C_k$ auf dem invarianten Torus, diese entsprechen jedoch keinen
physikalischen Bahnen. Ausgedrückt in den Wirkungs-Winkelvariablen
erhalten wir
\begin{align*}
  I_k &= \frac{1}{2\pi}\left[ S(\bm{q}) \right]_{C_k} = \frac{1}{2 \pi} \oint_{C_k} \bm{p} \diff \bm{q}, \\
  \alpha_k &= \frac{1}{2}\left[ \sigma_{\bm{q}} \right]_{C_k},
\end{align*}
wobei wir alle Größen beliebig wählen, mit Ausnahme eines festen
Winkels $\varphi_k$. Die Größe $\alpha_k$ entspricht hierbei dem
\acct{Maslov-Index}.

Die lokale Wellenfunktion wird definiert als
\begin{align*}
  \psi(\bm{q},R)  &=
                   \begin{cases}
                     B(\bm{q}) \ee^{\ii(S(\bm{q})/\hbar - \sigma_{\bm{q}}\pi/4)} ,&\bm{q}\in \mathcal R \\
                     0 ,&\text{sonst.}
                   \end{cases}
\end{align*}
und auf dem $p$-Zweig
\begin{align*}
  \overline{\psi}(\bm{p},R) &= \frac{1}{(2 \pi \hbar)^{N/2}} \int \diff^Nq \psi(\bm{q},R) \ee^{-\ii \bm{p}\cdot \bm{q}/\hbar} \nonumber \\
                            &=              
                              \begin{cases}
                                \overline{B}(\bm{p}) \ee^{\ii(\overline{S}(\bm{p})/\hbar - \sigma_{\bm{p}}\pi/4)} ,&\bm{p}\in \overline{\mathcal R} \\
                                0 ,&\text{sonst.}
                              \end{cases}
\end{align*}
Mit den einzelnen Einträgen
\begin{align*}
  \overline{S}(\bm{p}) &= S(\bm{q}) - \bm{p}\cdot\bm{q},\\
  \overline{B}(\bm{p}) &= B(\bm{q}) \cdot \left(\left\lvert\det\left( \frac{\partial^2 S}{\partial q_i \partial q_j}  \right)\right\rvert \right)^{-1/2}, \\
  \frac{\partial S}{\partial q_i} &= p_i, \\
  \sigma_{\bm{p}} &= \sigma_{\bm{q}} -\mathrm{sgn}\left(\frac{\partial p_i}{\partial q_j} \right).
\end{align*}

\begin{notice}[Anmerkung zu $B(\bm{q})$:]
  Erfüllt Kontinuitätsgleichung:
  \begin{align*}
    B(\bm{q}) &= \lvert \psi(\bm{q})\rvert^2 = \varrho(\bm{q}),\\
    \nabla(\varrho \bm{v}) &= 0, \quad \bm{v} = \frac{\bm{p}}{m}, \\
    \frac{\diff \varrho }{\diff t} &= \bm{v}\cdot\nabla\varrho = - \varrho \nabla\cdot \bm{v}.
  \end{align*}
  Die Lösung ist
  \begin{align*}
    \varrho(\bm{q}) &= \varrho_0(\omega_0) \frac{J(0,\omega_0)}{J(t,\omega_0)},\\
    J(t,\omega_0) &= \left| \det\left(\frac{\partial \bm{q}(t,\omega_0)}{\partial (t,\omega_0)} \right)  \right|, 
  \end{align*}
  Hierbei beschreibt $t$ die Zeit der Bewegung entlang der Bahn und
  $\omega_0$ $N-1$ Koordinaten, welche die Anfangsfläche einer
  Lagrangschen Mannigfaltigkeit definieren (d.h.\ die Mannigfaltigkeit
  senkrecht zur Bahn).
\end{notice}

\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \node  at (0,2) {(a)};
      \draw (2,-1)--(2,1);
      \draw[MidnightBlue,->] (0,0.75)--(2,0.75); 
      \draw[MidnightBlue,->](2,0.5)--(0,0.5); 
      \draw[MidnightBlue,->] (0,-0.75)--(2,-0.75); 
      \draw[MidnightBlue,->](2,-0.5)--(0,-0.5); 
    \end{scope}
    \begin{scope}[shift={(3.5,0)}]
      \node  at (0,2) {(b)};
      \draw (2.15,-1)--(2.15,1);
      \draw[MidnightBlue,out=0,in=90] (0,0.75) to (2,0);
      \draw[MidnightBlue,->,out=-90,in=0] (2,0) to (0,-0.75);
      \draw[MidnightBlue,out=0,in=90] (0,0.75+0.2) to (2,0+0.2);
      \draw[MidnightBlue,->,out=-90,in=0] (2,0+0.2) to (0,-0.75+0.2);
      \draw[MidnightBlue,out=0,in=90] (0,0.75-0.2) to (2,0-0.2);
      \draw[MidnightBlue,->,out=-90,in=0] (2,0-0.2) to (0,-0.75-0.2);
    \end{scope}
  \end{tikzpicture}
  \caption{Reflexion an einer (a) Potentialwand und (b) in der Nähe
    einer Potentialwand. }
  \label{fig:7.5.15-1}
\end{figure}
Wie in Abbildung~\ref{fig:7.5.15-1} (a) gilt für die Reflexion an
einer Potantialwand (Reflexion wie im Eindimensionalem)
\begin{align*}
  \frac{\partial \bm{q}}{\partial t} &= 0,
\end{align*}
wobei dann die Determinante in $J$ verschwindet,woraus die Divergenz
von $B(\bm{q})$ folgt.

Für das Szenario in Abbildung~\ref{fig:7.5.15-1} (b) ist
$\partial q / \partial t$ zu keinem Zeitpunkt null, aber die
Determinante verschwindet in der Nähe der Potentialwand. Die Kaustik
zeigt sich gerade durch $\det J = 0$. Sowie die Divergenz von
$B(\bm{q})$.

Im Fall $N=1$, d.h.\ im Eindimensionalem gilt
\begin{align*}
  J(t) &= \frac{\partial q}{\partial t} = p ,\\
  \implies \varrho(\bm{q}) &= \frac{1}{|\bm{p}|},\\
  \implies B(\bm{q}) &= \frac{1}{\sqrt{|p|}}.
\end{align*}
Die Fortsetzung der Wellenfunktion an den Kaustiken (durch geeigneten
Wechsel zwischen Orts- und Impulsraum) liefert die globale
semiklassische Wellenfunktion. Diese muss eine eindeutige Funktion auf
den invarianten Torus sein. Insbesondere muss $\psi(\bm{q})$ denselben
Wert nach ganzzahligem Umlauf entlang jedem der $N$ unabhängigen Wege
$C_k$ besitzen. Es folgt die semiklassische Quantisierungsbedingung
(Torus- oder EBK-Quantisierung)
\begin{align*}
  \frac{1}{\hbar} [S(\bm{q})]_{C_k} - [\sigma_{\bm{q}}]_{C_k} \frac{\pi}{4} &=2 \pi n_k, \quad n_k \in \mathbb{N}_0, \quad  k\in\{1,\ldots,N \},\\
  \implies I_k &= \frac{1}{2\pi}[S(\bm{q})]_{C_k} = \frac{1}{2\pi}\oint_{C_k} \bm{p}\cdot\diff\bm{q} \\
  &=\hbar  \left( n_k + \frac{\alpha_k}{4} \right), \\
  \alpha_k &= \frac{[\sigma_{\bm{q}}]_{C_k}}{2}.
\end{align*}
Auf jedem Torus ist die Energie konstant. Klassisch gilt für die
Energie $E(\bm{I}) = E(I_1,\ldots,I_N)$. Somit folgen die
quantisierten semiklassischen Energieeigenwerte
\begin{align*}
  E(\bm{n}) &= E\left(\hbar\left(\bm{n}+ \frac{\bm\alpha}{4}\right)\right),
\end{align*}
wobei der Index $\bm{n}$ einen Satz von $N$ Quantenzahlen für jeden
Zustand beschreibt. Somit zeigt sich, dass nur bestimmte Tori
semiklassisch erlaubt/relevant sind.

\begin{example}
  Im Folgenden betrachten wir einige Beispiele von separablen
  Systemen:
  \begin{enumerate}
  \item Zentralkraft-/Coulombpotential: Es gilt die Hamiltonfunktion
    \begin{align*}
      H &= \frac{1}{2M} \left(p_r^2 + \frac{p_\vartheta^2}{r^2} + \frac{p_\varphi^2}{r^2 \sin^2\vartheta}  \right) +V(r) = 0, \quad V(r) = -\frac{k}{r}.
    \end{align*}
    Für diese Art von Problemen bietet sich ein Separationsansatz der From
    \begin{align*}
      S &= S_r(r) + S_\vartheta(\vartheta) + S_\varphi(\varphi)
    \end{align*}
    an. Somit erhalten wir
    \begin{align*}
      \frac{\partial S_\varphi}{\partial \varphi} &= p_\varphi = L_z, \\
      \frac{\partial S_\vartheta}{\partial \vartheta} &= \left(L^2 - \frac{L_z^2}{\sin^2\vartheta}\right)^{1/2} \quad \text{mit} \quad L^2 = p_\vartheta^2 + \frac{p_\varphi^2}{\sin^2\vartheta},\\
      \frac{\partial S_r}{\partial r} &= \left(2 M E - \frac{L^2}{r^2} - V \right)^{1/2}.
    \end{align*}
    Die Wirkungsvariablen sind
    \begin{align*}
      I_\varphi &= \frac{1}{2\pi} \int_{0}^{2\pi} L_z \diff \varphi = m \hbar \qquad \text{Rotation $\alpha_\varphi = 0$} \\
      I_\vartheta & =\frac{1}{2\pi}\oint \sqrt{ L^2 - \frac{L_z^2}{\sin^2\vartheta} } \diff \vartheta = |\bm{L}| - |m|\hbar \\
                &= \left(\ell + \frac{1}{2} - |m| \right) \hbar, \quad \ell = |m|,|m|+1,\ldots,
    \end{align*}
    wobei $m$ eine ganze Zahl ist und aufgrund der Vibration gilt
    $\alpha_{\vartheta} = 2$ gilt. Weiter erhalten wir
    \begin{align*}
      |\bm{L}| &= \hbar \left( \ell +\frac{1}{2} \right) \\
      L^2 &= \left[ \ell(\ell+1) + \frac{1}{4}  \right]\hbar^2 
            \intertext{Die exakte quantenmechanische Rechnung ergibt $L^2 = \ell(\ell+1)\hbar^2$. Zudem folgt}
            I_r &= \frac{1}{2\pi} \oint \left(2ME - \frac{L^2}{r^2} + \frac{2Mk}{r} \right)^{1/2} \diff r \\
               &= -(I_\vartheta + |I_\varphi|) + k \sqrt{\frac{M}{2|E|}} \\
               &\stackrel{!}{=} \left(n_r + \frac{1}{2}\right) \hbar 
 \intertext{mit $n=n_r +\ell +1$} 
               &= \left(n - \left(\ell+\frac{1}{2}\right) \right) \hbar, \quad n=\ell+1,\ell+2,\ldots
    \end{align*}
    Umschreiben der Hamiltonfunktion führt zur Energie (Rydbergformel)
    \begin{align*}
      H = E = - \frac{Mk^2}{2(I_r + I_\vartheta + |I_\varphi|)^2} = - \frac{Mk^2}{2 \hbar^2 n^2}, \quad n\in\mathbb{N}.
    \end{align*}
  \item Starkeffekt
  \item Kreisbillard
  \end{enumerate}
\end{example}


\begin{notice}
  \begin{itemize}
  \item Die Torusquantisierung integrabler, aber nicht-separabler
    Systeme ist in der Praxis schwierig, da die Berechnung der
    Integrale $\oint_{C_k}\bm{p}\cdot\diff\bm{q}$ die Kenntnis der
    unabhängigen Wege $C_k$ erfordert.
  \item Die Quantisierungsmethode ist nicht anwendbar auf
    nicht-integrable (chaotische) Systeme (Einstein 1917). Hierbei ist
    die Integrabilität gerade Voraussetzung für die
    Torusquantisierung. Es verbleibt jedoch die Frage, wie chaotische
    Systeme semiklassisch quantisiert werden.
  \end{itemize}
\end{notice}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: