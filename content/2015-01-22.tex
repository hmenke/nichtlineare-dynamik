\begin{notice}
  \begin{itemize}
  \item Die Bewegungsgleichungen für $\mu(\tau)$, $\nu(\tau)$,
    $p_\mu(\tau)$ und $p_\nu(\tau)$ erhält man aus den Hamiltonschen
    Gleichungen (ohne Singularitäten).
  \item Der Kopplungsterm ist groß für große $B$ oder $E \nearrow 0$.
    Für klassische Bahnen genügt es $B=1$ zu setzen und allein die
    Energie $E$ zu variieren.
  \end{itemize}
\end{notice}

Die Untersuchung der Dynamik im Phasenraum erfolgt über
Poincaré-Schnitte.  Wähle dazu die Schnittebene $\nu=0$, damit $\Sigma
= \{\mu,p_\mu\}$.  Ohne den Kopplungsterm erhalten wir wieder die
Keplerellipsen.  Jeder Punkt in $\Sigma$ ist ein Fixpunkt
(periodische Bahn).

Frage: Was geschieht beim Einschalten einer nicht-integrablen Störung?
PSOS bei $E \lesssim \num{-0.6}$: Torusstrukturen ähnlich wie bei
einem integrablen System.  Diskussion der Fixpunkte:
\begin{itemize}
\item Elliptische Fixpunkte: Stabile Bahnen (Bahnen parallel und
  senkrecht zum Magnetfeld)
\item Hyperbolische Fixpunkte: Instabile Bahnen (fast kreisförmige
  Bahn)
\end{itemize}

Frage: Wie lassen sich die bei schwachen nicht-integrablen Störungen
existierenden regulären Strukturen im PSOS erklären?  Dies führt uns
auf die \acct{klassische Störungstheorie} für das Wasserstoffatom im
Magnetfeld ($L_z=0$).
\begin{gather*}
  E = E_0 + E_1 \text{ mit } E_0 \ll 0, E_1 \text{ klein (Störung)} \\
  H = \underbrace{\frac12 (p_\mu^2 + p_\nu^2) - E_0(\mu^2 + \nu^2)}_{H_0}
  \underbrace{- E_1(\mu^2 + \nu^2) + \frac18 \mu^2 \nu^2 (\mu^2 + \nu^2)}_{\varepsilon H_1}
  = 2
\end{gather*}
\begin{enumerate}
\item Löse die Hamilton-Jacobi-Gleichung für das ungestörte System $H_0=2$:
  \begin{equation*}
    p_\mu = \frac{\partial S_\mu}{\partial \mu}\;,\quad
    p_\nu = \frac{\partial S_\nu}{\partial \nu}\;,\quad
    \text{Separationsansatz}\quad
    S = S_\mu(\mu) + S_\nu(\nu) \;,\quad
    \omega\equiv\sqrt{-2E_0}
  \end{equation*}
  Damit
  \begin{align*}
    &\underbrace{\frac12 \left(\left(\frac{\partial S_\mu}{\partial \mu}\right)^2 + \omega^2 \mu^2\right)}_{\alpha_\mu}
    + \underbrace{\frac12 \left(\left(\frac{\partial S_\nu}{\partial \nu}\right)^2 + \omega^2 \nu^2\right)}_{\alpha_\nu}
    = 2 \\
    &\frac{\partial S_\mu}{\partial \mu} = \sqrt{2\alpha_\mu - \omega^2 \mu^2}
  \end{align*}
  Wirkungsvariable:
  \begin{align*}
    J_1 &= \frac{1}{2\pi} \oint \sqrt{2\alpha_\mu - \omega^2 \mu^2} \diff\mu
    = \frac{1}{\pi} \int_{\mu_{\text{min}}}^{\mu_{\text{max}}} \sqrt{2\alpha_\mu - \omega^2 \mu^2} \diff\mu
    = \frac{\alpha_\mu}{\omega} \\
    J_2 &= \frac{\alpha_\nu}{\omega}
  \end{align*}
  Winkelvariable:
  \begin{align*}
    \frac{\partial S_\mu}{\partial \mu} &= \sqrt{2\omega J_1 - \omega^2 \mu^2} \\
    \theta_1 = \frac{\partial S}{\partial J_1} &= \int \frac{1}{\sqrt{2 J_1/\omega - \mu^2}} \diff\mu
    = \arcsin\left(\sqrt{\frac{\omega}{2 J_1}} \mu\right)
  \end{align*}
  Damit
  \begin{align*}
    \mu &= \sqrt{2 J_1/\omega} \sin\theta_1 &
    p_\mu &= \sqrt{2 J_1\omega} \cos\theta_1 \\
    \nu &= \sqrt{2 J_2/\omega} \sin\theta_2 &
    p_\nu &= \sqrt{2 J_2\omega} \cos\theta_2 \\
    H_0 &= \omega(J_1+J_2)
  \end{align*}
  Frequenzentartung:
  \begin{equation*}
    \frac{\partial H_0}{\partial J_1} = \frac{\partial H_0}{\partial J_2} = \omega
  \end{equation*}

\item Neue Wirkungs-Winkel-Variablen zur Unterteilung in schnelle und
  langsame Bewegung.  Die kanonische Transformation lautet $F_2 =
  (\theta_1 - \theta_2) \hat J_1 + \theta_2 \hat J_2$
  \begin{align*}
    J_1 &= \hat J_1 \\
    J_2 &= \hat J_2 - \hat J_1 \\
    \hat\theta_1 &= \theta_1 - \theta_2 \\
    \hat\theta_2 &= \theta_2 \\
    \mu &= \sqrt{2 \hat J_1/\omega} \sin(\hat\theta_1 + \hat\theta_2) \\
    \nu &= \sqrt{2 (\hat J_2 - \hat J_1)/\omega} \sin\hat\theta_2 \\
    p_\mu &= \sqrt{2 \hat J_1/\omega} \cos(\hat\theta_1 + \hat\theta_2) \\
    p_\nu &= \sqrt{2 (\hat J_1-\hat J_2)} \cos\hat\theta_2
  \end{align*}
  Damit: $H_0 = \omega \hat J_2 = 2$ (Schnelle Bewegung in $\theta_2$
  mit Frequenz $\omega$, $\hat\theta_1=\const$).

\item Schreibe die Gesamt-Hamiltonfunktion in den
  Wirkungs-Winkel-Variablen der ungestörten Bewegung.
  \begin{multline*}
    H = \omega \hat J_2 - \left[
      E_1 - \frac12 \hat J_1(\hat J_2-\hat J_1) \frac{1}{\omega^2}
      \sin^2(\hat\theta_1+\hat\theta_2) \sin^2\hat\theta_2
    \right] \\
    \times \frac{2}{\omega} \left[
      \hat J_1 \sin^2(\hat\theta_1+\hat\theta_2) + (\hat J_2-\hat J_1)
      \sin^2\hat\theta_2
    \right]
    = 2
  \end{multline*}
  Exakt, aber weder $\hat\theta_1$ noch $\hat\theta_2$ sind zyklisch.

\item Mittelung von $H$ über die schnelle Winkel-Variable
  $\hat\theta_2$.  Benutze
  \begin{gather*}
    \frac{1}{2\pi} \int_0^{2\pi} \sin^4(x+\alpha) \sin^2x \diff x
    = \frac{1}{2\pi} \int_0^{2\pi} \sin^2(x+\alpha) \sin^4x \diff x
    = \frac{1}{16} (1+4\cos^2\alpha) \\
    H \simeq \omega\hat J_2 - \frac{1}{\omega} E_1 \hat J_2 + \frac{1}{16\omega^3} (\hat J_2-\hat J_1) \hat J_1\hat J_2 (1+4\cos^2\hat\theta_1)
    = 2
  \end{gather*}
  $\hat\theta_2$ zyklisch.
  \begin{align*}
    H' &= H-2 \simeq -\frac{2}{\omega^2} E_1 + \frac{1}{8\omega^4}\biggl(\underbrace{\frac{2}{\omega}-\hat J_1}_{J_2}\biggr) \underbrace{\hat J_1}_{J_1}(1+4\cos^2\underbrace{\hat\theta_1}_{\mathclap{\theta_1-\theta_2}})=0 \\
    16 \omega^2 E_1 &= J_1 J_2 (1+4\cos^2(\theta_1-\theta_2)) \simeq \const
  \end{align*}
  Somit haben wir die adiabatische Invariante in
  Wirkungs-Winkel-Variablen gefunden. Adiabatische Invarianten sind
  nur innerhalb der Störungsrechnung gültig.

\item Analytischer Poincaré-Schnitt: Einsetzen der ursprünglichen
  (semiparabolischen) Koordinaten liefert adiabatische Invariante.
  \begin{equation*}
    5 (p_\mu p_\nu + \omega^2 \mu\nu)^2 + \omega^2 (p_\mu\nu-p_\nu\mu)^2 = 64 \omega^4 E_1 = \const
  \end{equation*}
  PSOS bei $\nu=0$:
  \begin{equation*}
    5 (p_\mu^2 + \omega^2 \mu^2) p_\nu^2 = 64 \omega^4 E_1 = \const
  \end{equation*}
  Benutze: $p_\nu \nu^2 = 4 + 2E \mu^2 - p_\mu^2$ (aus $H=2$)
  \begin{equation*}
    \boxed{
      5( p_\mu^2 - 2 E_0 \mu^2)(4 - p_\mu^2 + 2 E \mu^2) = \const
    }
  \end{equation*}
  Interpretation der Dynamik im \enquote{fast-integrablen} Bereich:
  Sekularbewegung der Keplerellipsen: \enquote{langsame} periodische
  Bewegung von Drehimpuls $\bm L = \bm r \times \bm p$ und
  Runge-Lenz-Vektor $\bm A = \bm p \times \bm L - \frac{\bm r}{|\bm
    r|}$.
\end{enumerate}

\subsection{Klassische Störungstheorie für Hamiltonsche Systeme mit $N$
  Freiheitsgraden}

Sei $H(\bm J,\bm\theta) = H_0(\bm J) + \varepsilon H_1(\bm
J,\bm\theta)$ mit $(\bm J,\bm\theta)\in\mathbb{R}^{2N}$ die
Wirkungs-Winkel-Variablen eines integrablen Systems $H_0$ und
$\varepsilon H_1$ eine kleine Störung ($\varepsilon$ klein).

Ziel: Wir suchen neue Wirkungs-Winkel-Variablen $(\bm{\hat
  J},\bm{\hat\theta})$, sodass (bis auf Terme der Ordnung
$\varepsilon^2$) die neue Hamiltonfunktion nur von den neuen
Wirkungs-Variablen abhängt: $H(\bm J,\bm\theta)\to \hat H(\bm{\hat{J}})
+ \mathcal O(\varepsilon^2)$.

Fourierentwicklung der Störung:
\begin{equation*}
  H_1(\bm J,\bm\theta) = \sum_{\bm m \neq 0} H_{1,\bm m}(\bm J) \ee^{\ii \bm m \cdot \bm\theta}
  \;,\quad\text{mit } \bm m \in \mathbb{Z}^N, \bm m\cdot\bm\theta=\sum_{i=1}^N m_i \theta_i
\end{equation*}
Ansatz für Erzeugende einer kanonischen Transformation:
\begin{equation*}
  S(\bm{\hat{J}},\bm\theta) = \bm{\hat{J}} \cdot \bm\theta + \varepsilon \sum_{\bm m} S_{\bm m}(\bm{\hat{J}}) \ee^{\ii\bm m\cdot\bm\theta} + \mathcal O(\varepsilon^2)
\end{equation*}
Damit:
\begin{equation*}
  \bm J = \frac{\partial S}{\partial \bm\theta} = \bm{\hat{J}} + \ii\varepsilon \sum_{\bm m} \bm m S_{\bm m}(\bm{\hat{J}}) \ee^{\ii\bm m\cdot\bm\theta} + \mathcal O(\varepsilon^2)
\end{equation*}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End:
