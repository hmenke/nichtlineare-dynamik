\subsection{Die Bewegung im Phasenraum, Poincaré-Schnitte}

Aus den Hamiltonschen Gleichungen folgt für autonome Systeme:
\begin{enumerate}
\item Trajektorien im Phasenraum schneiden sich nicht.
\item Ein Gebiet mit Grenze $C_1(t=t_1)$ geht über in ein Gebiet mit
  Grenze $C_2(t=t_2)$ (vgl.\ Abbildung~\ref{fig:15-1-15-0}).
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx]
      \draw[<->] (0,2) node[left] {$p$} |- (3,0) node[right] {$q$};
      \draw[MidnightBlue] (.6,.5) ellipse (.3 and .4);
      \node[MidnightBlue,right] at (.9,.2) {$C_1(t=t_1)$};
      \draw[MidnightBlue] (2.6,1.5) ellipse (.3 and .4);
      \node[MidnightBlue,above] at (2.6,1.9) {$C_2(t=t_2)$};
      \draw[DarkOrange3,arrow inside] (.5,.6) node[dot] {} to[bend left] (2.5,1.7) node[dot] {};
      \draw[DarkOrange3,arrow inside] (.8,.5) node[dot] {} -- (2.4,1.5) node[dot] {};
      \draw[DarkOrange3,arrow inside] (.6,.3) node[dot] {} to[bend right=10] (2.7,1.3) node[dot] {};
    \end{tikzpicture}
    \caption{Ein Gebiet mit Grenze $C_1(t=t_1)$ geht über in ein
      Gebiet mit Grenze $C_2(t=t_2)$.}
    \label{fig:15-1-15-0}
  \end{figure}
\item Das Phasenraumvolumen bleibt konstant (Satz von Liouville)
\end{enumerate}
Das Phasenraumportrait ist $2N$-dimensional, also nur wirklich
sinnvoll für eindimensionale Systeme wie ein Pendel.  Besser geeignet
zur Analyse von Systemen mit zwei oder mehr Freiheitsgraden sind
\acct[Pincaré-Schnitt]{Poincaré-Schnitte}.  Betrachte die Bewegung
eines integrablen Systems ($N=2$) in Wirkungs-Winkel-Variablen.
\begin{align*}
  \theta_i = \omega_i t + \beta_i \quad\text{(Tori im Phasenraum)}
\end{align*}
Die $\theta_1$-Bewegung ist periodisch mit der Periode $T_1 =
2\pi/\omega_1$.  Betrachte $(J_2,\theta_2)$ zu den Zeiten $t_n = n
T_1$ (Stroboskopische Abbildung).

Für beliebige Systeme ohne Kenntnis von Wirkungs-Winkel-Variablen
wählen wir eine Schnitt\-ebene $\Sigma$ (PSOS, Poincaré surface of
section) im Phasenraum.
\begin{itemize}
\item Reguläre (integrable) Dynamik: PSOS zeigt (defomierte) Tori.
\item Irreguläre (chaotische) Dynamik: Eine Trajektorie füllt ein
  flächenhaftes Gebiet im PSOS aus (d.h.\ weniger als $N=2$
  Erhaltungsgrößen bzw.\ Integrale der Bewegung).
\end{itemize}
\begin{example}
  Hénon-Heiles-Problem:
  \begin{equation*}
    H = \underbrace{
      \frac12 (p_x^2 + p_y^2) + \frac12 (x^2 + y^2)
    }_{\text{2D Harmonischer Oszillator}}
    + \underbrace{
      x^2 y - \frac13 y^3
    }_{\text{Kopplung}} = E
  \end{equation*}
\end{example}
Allgemein ist die Dimension des PSOS $2N-2$.

\subsection{Das Wasserstoffatom im Magnetfeld}

\begin{figure}[tb]
  \centering  
  \begin{tikzpicture}[gfx,x={(1cm,0cm)},y={(-.5cm,-.5cm)},z={(0cm,1cm)}]
    \node[dot,MidnightBlue] at (0,0,1) {};
    \draw plot[domain=0:20,samples=51,smooth] ({cos(\x r)},{sin(\x r)},\x/10);
    \draw[->,DarkOrange3] (2,0,.5) -- node[right] {$\bm B = B \bm e_z$} (2,0,1.5);
  \end{tikzpicture}
  \caption{Wasserstoffatom im Magnetfeld}
\end{figure}

Klassisch gesehen bewegt sich das Elektron unter dem Einfluss von zwei
Kräften, der Coulomb-Kraft und der Lorentz-Kraft
(geschwindigkeitsabhängig):
\begin{align*}
  \bm F_C &= - \frac{k \bm r}{r^3} \\
  \bm F_L &= - e \bm\omega \times \bm B
\end{align*}
Die Hamilton-Jacobi-Gleichung ist nicht separabel in keinem
Koordinatensystem.  Das System ist ein Prototyp eines realen
physikalischen (Quanten-)Systems mit klassischem Chaos.

Aus der Elektrodynamik ist bekannt:
\begin{equation*}
  \bm B = \nabla\times\bm A
\end{equation*}
mit dem Vektorpotential in symmetrischer Eichung
\begin{equation*}
  \bm A = \frac12 \bm B \times \bm r
\end{equation*}
Im Hamiltonformalismus berücksichtigen wir Magnetfelder (Lorentzkraft)
durch die minimale Substitution.  Die Hamiltonfunktion in atomaren
Einheiten\footnote{Atomare Einheiten (a.u.): $\hbar = e = m_e =
  (4\pi\varepsilon_0)^{-1} = 1$} lautet
\begin{align*}
  H
  &= \frac12 (\bm p + \bm A)^2 - \frac1r \\
  &= \frac12 \bm p^2 - \frac1r
  + \frac12 \bm B \cdot \underbrace{(\bm r\times\bm p)}_{\text{Drehimpuls $\bm L$}}
  + \frac1\varrho (\bm B\times\bm r)^2 \\
  &\stackrel{\mathllap{\text{mit $\bm B = B \bm e_z$}}}{=}
  \frac12 \bm p^2 - \frac1r
  + \underbrace{\frac12 B L_z}_{\mathclap{\text{paramagn.\ Term}}}
  + \overbrace{\frac18 B^2 \underbrace{(x^2 + y^2)}_{\varrho^2}}^{\text{diamagn.\ Term}}=E
\end{align*}
und hat zwei gekoppelte Freiheitsgrade $\varrho$ und $z$.  Das System
ist nicht separabel.  Die Hamiltonschen Gleichungen lassen sich nur
numerisch lösen.  Das Problem ist die Coulomb-Singularität
(Beschleunigung divergiert im Ursprung).  Abhilfe schafft die
Regularisierung der Coulomb-Singularität in semiparabolischen
Koordinaten.
\begin{equation*}
  \mu = \sqrt{r+z} \;,\quad
  \nu = \sqrt{r-z} \quad
  \text{($\varphi$-Bewegung schon absepariert)}
\end{equation*}
Im Folgenden ist $L_z = 0$.
\begin{equation*}
  H = \frac12 \frac{1}{\mu^2+\nu^2} (p_\mu^2 + p_\nu^2)
  - \frac{2}{\mu^2 + \nu^2} + \frac18 B^2 \mu^2 \nu^2 = E
\end{equation*}
Zusätzlich führt man eine Transformation der Zeit durch
\begin{equation*}
  \diff t = 2 r \diff\tau
\end{equation*}
Ohne Herleitung erhält man dann
\begin{equation*}
  \boxed{
    \mathcal{H} = 
    \underbrace{\frac12 (p_\mu^2 + p_\nu^2) - E(\mu^2 + \nu^2)}_{\text{2D harm.\ Osz.\ (für $E<0$)}}
    + \underbrace{\frac18 B^2 \mu^2 \nu^2 (\mu^2 + \nu^2)}_{\text{Kopplungsterm}}
    = 2
  }
\end{equation*}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: