Wir haben bisher den Weg ins Chaos über ein integrables System mit
einer Störung betrachtet.  Für eine kleine Störung gilt das
KAM-Theorem.  Nach diesem überleben fast alle Tori, bis auf die
Nullmenge der resonanten Tori.  Bei Zunahme der Störung verletzen mehr
und mehr Tori die Bedingung der genügenden Irrationalität und der
Anteil der Resonanzlücken wächst.  Nach dem Poincaré-Birkhoff-Theorem
zeigt der PSOS eine gerade Zahl von Fixpunkten (elliptisch,
hyperbolisch) in den Resonanzlücken ($N=2$).  Der Phasenraum, bzw.\
der PSOS bekommt eine fraktale Struktur. Siehe hierzu
Abbildung~\ref{fig:16.4.15-00}.  Nimmt die Störung noch weiter zu,
entstehen stochastische Gebiete in der Umgebung hyperbolischer
Fixpunkte.  Die stabilen und instabilen Mannigfaltigkeiten $W_s$ und
$W_u$ schneiden sich unendlich oft in den homoklinen oder heteroklinen
Punkten.  Bei noch weiterer Zunahme der Störung lösen sich immer mehr
KAM-Tori auf und die stochastischen Gebiete wachsen weiter.
Schließlich löst sich der letzte KAM-Torus und es findet ein Übergang
zu globaler Stochastizität statt (vollständiges Chaos).  Im PSOS sind
keine Strukturen mehr zu erkennen.  Die höchste Stufe von Chaos wäre
dann ein ergodisches Verhalten.  Die Dynamik heißt ergodisch, falls
fast jede Trajektorie jeden Punkt im Phasenraum beliebig oft beliebig
nah kommt.  Das Zeitmittel entspricht dann dem Mittel über das
Phasenraumvolumen.


\subsection{Stabilität klassischer Bahnen, Monodromiematrix}

Bislang haben wir unter Chaos die Auflösung der Torusstrukturen
(Integrale der Bewegung) im Phasenraum verstanden.  Ein anderer Zugang
erfolgt über die sensitive Abhängigkeit der Bewegung von den
Anfangsbedingungen, wie wir sie schon bei den diskreten Abbildungen
(Maps) betrachtet haben und führt auf die Unvorhersagbarkeit der
Langzeitdynamik instabiler Bahnen.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[MidnightBlue] (0,-.1) node[dot,label={below:$\bm\gamma(0)$}] {}
    to[out=0,in=160] (3,-.5) node[dot,label={below:$\bm\gamma(t)$}] {};
    \draw[DarkOrange3] (0,.1) node[dot,label={above:$\bm\gamma(0)+\Delta\bm\gamma(0)$}] {}
    to[out=0,in=200] (3,.8) node[dot,label={above:$\bm\gamma(t)+\Delta\bm\gamma(t)$}] {};
  \end{tikzpicture}
  \caption{Zwei Bahnen mit unterschiedlichen Anfangsbedingungen entfernen sich exponentiell voneinander.}
  \label{fig:2015-04-16-1}
\end{figure}

Gesucht ist eine charakteristische Aussage über $\Delta\bm\gamma(t)$ in
Abbildung~\ref{fig:2015-04-16-1}.  Eine Idee ist die Linearisierung
von $\Delta\bm\gamma(t)$.
\begin{equation*}
  \Delta\bm\gamma(t) = \mathscr M(0,t) \cdot \Delta\bm\gamma(0)
\end{equation*}
mit der Stabilitätsmatrix $\mathscr M(0,t) \in \mathbb R^{2N\times2N}$ und dem
Phasenraumvektor
\begin{equation*}
  \bm\gamma =
  \begin{pmatrix}
    \bm q \\
    \bm p
  \end{pmatrix}
  \in \mathbb R^{2N} \; .
\end{equation*}
Die Trajektorie $\bm\gamma(t)$ erhalten wir aus den Hamiltonschen
Bewegungsgleichungen.
\begin{equation*}
  \dot{\bm\gamma}(t) = \mathscr J \frac{\partial H}{\partial \bm\gamma}
  \quad\text{mit}\quad
  \mathscr J =
  \begin{bmatrix}
    0 & \mathds 1 \\
    - \mathds 1 & 0 \\
  \end{bmatrix} \; .
\end{equation*}
Die symplektische Matrix $\mathscr J$ hat die Eigenschaften
$\mathscr J^{-1} = - \mathscr J = \mathscr J^\T$ und
$\mathscr J^2 = - \mathds 1$.

Aus der Hamiltonschen Dynamik und dem Satz von Liouville folgt, dass
$\mathscr M(0,t)$ eine symplektische Matrix ist.

\begin{notice}[Einschub zur symplektischen Gruppe:]
  Die reelle symplektische Gruppe $\mathrm{SP}_{2N}(\mathbb R)$ ist
  die Menge der Matrizen $\mathscr M$ mit
  \begin{equation*}
    \boxed{\mathscr M^\T \mathscr J \mathscr M = \mathscr J \; .}
  \end{equation*}
  Aus dieser Gleichung ergeben sich einige Eigenschaften:
  \begin{enumerate}
  \item Sei $\lambda$ Eigenwert von $\mathscr M$, dann ist auch
    $1/\lambda$ Eigenwert.  Beweis:
    \begin{align*}
      \mathscr M x &= \lambda x \\
      \implies \mathscr M^\T y &= \lambda y \\
      \implies (\mathscr M^\T)^{-1} y &= \frac{1}{\lambda} y \\
      \implies \mathscr J \mathscr M \mathscr J^{-1} y &= \frac{1}{\lambda} y \\
      \implies - \mathscr J \mathscr M \mathscr J y &= \frac{1}{\lambda} y \\
      \implies \mathscr M (\mathscr J y) &= \frac{1}{\lambda} (\mathscr J y)
    \end{align*}
    damit ist $1/\lambda$ Eigenwert von $\mathscr M$ mit Eigenvektor
    $\mathscr J y$.
  \item Es gilt $\det\mathscr M = +1$.  Beweis:
    \begin{align*}
      \mathscr M^\T \mathscr J \mathscr M &= \mathscr J \; , \quad
      \det\mathscr J = 1 \\
      \implies \det\mathscr M^\T &= \det\mathscr M \\
      \implies \det\mathscr M &= \pm 1
    \end{align*}
    Aus der vorigen Eigenschaft folgt, dass
    \begin{equation*}
      \det \mathscr M = \prod_{i=1}^N \lambda_i \cdot \frac{1}{\lambda_i} = +1 \; .
    \end{equation*}
  \item Für das charakteristische Polynom gilt
    \begin{align*}
      \chi(\lambda) = \det(\mathscr M - \lambda\mathds 1)
      = \sum_{n=0}^{2N} a_n \lambda^n
    \end{align*}
    mit den Koeffizienten $a_{2N} = a_0 =1 $.  Für alle anderen
    Koeffizienten gilt die Beziehung $a_n = a_{2N-n}$.  Beweis:
    Betrachte das Polynom
    \begin{align*}
      \bar\chi(\lambda) = \lambda^{2N} \chi(1/\lambda)
      = \sum_{n=0}^{2N} a_{2N-n} \lambda^n \; .
    \end{align*}
    Aus der ersten Eigenschaft folgt $\chi(\lambda)$ und
    $\bar\chi(\lambda)$ haben dieselben Nullstellen, außerdem $a_{2N}
    = a_0 = 1$, also müssen alle Koeffizienten übereinstimmen.  Daraus
    ergibt sich, dass $a_n = a_{2N-n}$.
  \end{enumerate}
\end{notice}

Ist $\mathscr M$ eine reelle Matrix folgt, dass $\chi(\lambda)$ nur
reelle Koeffizienten $a_n$ hat.  Ist der Eigenwert $\lambda_i$
komplex, dann ist $\bar\lambda_i$ ein Eigenwert von $\mathscr M$.  Die
Eigenwerte treten entweder als Dubletts oder Quartetts auf.
\begin{gather*}
  \lambda, \bar\lambda, \frac{1}{\lambda}, \frac{1}{\bar\lambda} \\
  \lambda, \frac{1}{\lambda} \text{ reell} \\
  \lambda = \ee^{\ii\varphi}, \frac{1}{\lambda} = \bar\lambda = \ee^{-\ii\varphi} \text{ mit } \varphi \in \mathbb R
\end{gather*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,square/.style={draw=DarkOrange3,rectangle,inner sep=3pt}]
    \draw[->] (-2,0) -- (2,0) node[below] {$\real\lambda$};
    \draw[->] (0,-2) -- (0,2) node[left] {$\imag\lambda$};
    \draw (0,0) circle (1);
    \draw plot[only marks,mark=x] coordinates {
      (.5,0) (1.5,0) (60:1) (-60:1) (1.5,1.5) (1.5,-1.5)};
    \node[square] at (.25,.5) {};
    \node[square] at (.25,-.5) {};
    \node[square] at (1.5,1.5) {};
    \node[square] at (1.5,-1.5) {};
  \end{tikzpicture}
  \caption{Auftreten der Eigenwerte symplektischer Matrizen in
    Dubletts oder Quartetts.}
  \label{fig:2015-04-16-2}
\end{figure}

Zurück zur Linearisierung:
\begin{equation*}
  \Delta\bm\gamma(t) = \mathscr M(0,t) \cdot \Delta\bm\gamma(0)
\end{equation*}
bzw.\ differentiell
\begin{equation*}
  \mathscr M_{ij}(0,t) = \frac{\partial \gamma_i(t)}{\partial \gamma_j(0)} \; .
\end{equation*}
Damit können wir die Stabilitätsmatrix $\mathscr M(0,t)$ aus einem
Differentialgleichungssystem bestimmen.
\begin{equation*}
  \| \mathscr M(0,t) \| = \sup|\lambda_i(0,t)|
\end{equation*}
mit dem Liapunov-Exponenten
\begin{equation*}
  L = \lim_{t\to\infty}\frac{\ln\| \mathscr M(0,t) \|}{t} \; .
\end{equation*}
Ist der Exponent $L=0$, so ist die Bahn stabil und der Abstand
benachbarter Trajektorien wächst algebraisch.  Für $L>0$ ist sie
instabil und der Abstand benachbarter Trajektorien wächst
exponentiell.  Für große Zeiten folgt die Unberechenbarkeit der
Dynamik.

Für periodische Bahnen ist der Liapunov-Exponent berechenbar aus nur
einem Umlauf ($t=T$), denn es gilt
\begin{align*}
  L &= \lim_{t\to\infty} \frac{\ln\| \mathscr M(0,t) \|}{t} \\
    &= \lim_{n\to\infty} \frac{\ln\| \mathscr M(0,nT) \|}{nT} \\
    &= \lim_{n\to\infty} \frac{\ln\| \mathscr M(0,T)^n \|}{n T} \\
    &= \frac{\cancel{n} \ln\| \mathscr M(0,T) \|}{\cancel{n} T} \; .
\end{align*}

\minisec{Periodische Bahnen}

Periodische Bahnen sind Fixpunkte in der Poincaré-Abbildung.  In
Systemen mit $N=2$ Freiheitsgraden betrachtet man lokale Koordinaten,
z.B.\ $(y,p_y)$ in der Umgebung eines Fixpunktes, d.h.\ $(y,p_y) =
(0,0)$ ist Fixpunkt der Abbildung.
\begin{equation*}
  \begin{pmatrix}
    y_{n+1} \\
    p_{y_{n+1}} \\
  \end{pmatrix}
  =
  \underbrace{
    \begin{bmatrix}
      m_{11} & m_{12} \\
      m_{21} & m_{22} \\
    \end{bmatrix}
  }_{\mathscr M}
  \begin{pmatrix}
    y_n \\
    p_{y_n} \\
  \end{pmatrix} \; .
\end{equation*}
Diese Map ist flächenerhaltend und die Matrix $\mathscr M \in \mathbb
R^{(2N-2) \times (2N-2)}$ heißt \acct{Monodromiematrix} und ist symplektisch.


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: